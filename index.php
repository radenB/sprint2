<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package thelawfirm
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="information">
				<div>
					<h1>Your Current Dance</h1>
					<hr>
					<ul>
						
					</ul>
				</div>
				<a href="<?php echo get_template_directory_uri(); ?>/img/Clipped_Sequence_1.mov" target="_blank"><div class="current-dance"></div></div></a>
			</div><div class="stepWrap">
				<div>
					<div id="step-01" class="step step-01" data-name="1" data-step="Balancé">
						<div class="add-step"></div>
						<div></div>
					</div>
					<div id="step-02" class="step step-02" data-name="2" data-step="Soutenu">
						<div class="add-step"></div>
						<div></div>
					</div>
				</div>
				<div>
					<div id="step-03" class="step step-03" data-name="3" data-step="Fouetté">
						<div class="add-step"></div>
						<div></div>
					</div>
					<div id="step-04" class="step step-04" data-name="4" data-step="Développé">
						<div class="add-step"></div>
						<div></div>
					</div>
				</div>
				<div>
					<div id="step-05" class="step step-05" data-name="5" data-step="Pirouette en dedans">
						<div class="add-step"></div>
						<div></div>
					</div>
					<div id="step-06" class="step step-06" data-name="6" data-step="Pas de bourée">
						<div class="add-step"></div>
						<div></div>
					</div>
				</div>
				<div>
					<div id="step-07" class="step step-07" data-name="7" data-step="Pirouette en dehors">
						<div class="add-step"></div>
						<div></div>
					</div>
					<div id="step-01" class="step step-08" data-name="8" data-step="Piqué to 1st arabesque">
						<div class="add-step"></div>
						<div></div>
					</div>
				</div>
				<div>
					<div id="step-09" class="step step-09" data-name="1" data-step="Chassé to 1st arabesque">
						<div class="add-step"></div>
						<div></div>
					</div>
				</div>
			</div>

			<!-- <div class="nextArrow"></div>
			<div class="prevArrow"></div> -->

			<div class="video-bar">
				<div class="progress-wrap">
					<h1>CURRENT DANCE PROGRESS</h1>
					<div class="progress-bg"></div>
					<div class="progress"></div>
				</div>
				<h5 class="share">SHARE DANCE</h5><h5 class="reset">RESET DANCE</h5>
			</div>

			<!-- <div class="complete-demo"></div> -->

			<div class="lightBox">
				<div id="step1" class="fullInfo">
					<div class="video" style="background: url('<?php echo get_template_directory_uri(); ?>/img/Move1.jpg') center no-repeat; background-size: cover;"></div>
					<div>
						<h1>Balancé</h1>
						<h4>(ba-lahn-SAY)</h4>
						<!-- <h5>	French, literally, teetotum<br/>
						First Known Use: 1706</h5> -->
						<p>Rocking step. The weight is shifted from one foot to the other.</p>
						<hr>
						<a href="javascript:void(0)"><h5>Suggested Music</h5></a>
						<a href="javascript: void(0)"><h5>Dance Example</h5></a>
					</div>

				</div>
				<div id="step2" class="fullInfo">
					<div class="video" style="background: url('<?php echo get_template_directory_uri(); ?>/img/Move2.jpg') center no-repeat; background-size: cover;"></div>
					<div>
						<h1>Soutenu</h1>
						<h4>(soot-NEW)</h4>
						<!-- <h5>	French, literally, teetotum<br/>
						First Known Use: 1706</h5> -->
						<p>Sustained in turning.</p>
						<hr>
						<a href="javascript:void(0)"><h5>Suggested Music</h5></a>
						<a href="javascript: void(0)"><h5>Dance Example</h5></a>
					</div>

				</div>
				<div id="step3" class="fullInfo">
					<div class="video" style="background: url('<?php echo get_template_directory_uri(); ?>/img/Move3.jpg') center no-repeat; background-size: cover;"></div>
					<div>
						<h1>Fouetté</h1>
						<h4>(fweh-tay)</h4>
						<!-- <h5>	French, literally, teetotum<br/>
						First Known Use: 1706</h5> -->
						<p>Whipped.</p>
						<hr>
						<a href="javascript:void(0)"><h5>Suggested Music</h5></a>
						<a href="javascript: void(0)"><h5>Dance Example</h5></a>
					</div>

				</div>
				<div id="step4" class="fullInfo">
					<div class="video" style="background: url('<?php echo get_template_directory_uri(); ?>/img/Move4.jpg') center no-repeat; background-size: cover;"></div>
					<div>
						<h1>Développé</h1>
						<h4>(davy- law-PAY)</h4>
						<!-- <h5>	French, literally, teetotum<br/> -->
						<!-- First Known Use: 1706</h5> -->
						<p>Developed. A developpe is a movement in which the working leg is drawn up and slowly extended to an open position en l’air and held there with perfect control.</p>
						<hr>
						<a href="javascript:void(0)"><h5>Suggested Music</h5></a>
						<a href="javascript: void(0)"><h5>Dance Example</h5></a>
					</div>

				</div>
				<div id="step5" class="fullInfo">
					<div class="video" style="background: url('<?php echo get_template_directory_uri(); ?>/img/Move5.jpg') center no-repeat; background-size: cover;"></div>
					<div>
						<h1>Pirouette en dedans</h1>
						<h4>(peer-WET)</h4>
						<!-- <h5>	French, literally, teetotum<br/>
						First Known Use: 1706</h5> -->
						<p>Pirouette - Whirl or spin. A complete turn of the body on one foot either on the pointe or demi- pointe, the motive power being obtained from a combination of plie and arm movement. There is a great variety of pirouettes. <br/> 
						en dedans - Inward. The leg moves in a circular direction, counter-clockwise from back to front.<br/>(ahn duh-DAHN)</p>
						<hr>
						<a href="javascript:void(0)"><h5>Suggested Music</h5></a>
						<a href="javascript: void(0)"><h5>Dance Example</h5></a>
					</div>

				</div>
				<div id="step6" class="fullInfo">
					<div class="video" style="background: url('<?php echo get_template_directory_uri(); ?>/img/Move6.jpg') center no-repeat; background-size: cover;"></div>
					<div>
						<h1>Pas de bourée</h1>
						<h4>(pah duh boo RAY)</h4>
						<!-- <h5>	French, literally, teetotum<br/>
						First Known Use: 1706</h5> -->
						<p>Beating steps.</p>
						<hr>
						<a href="javascript:void(0)"><h5>Suggested Music</h5></a>
						<a href="javascript: void(0)"><h5>Dance Example</h5></a>
					</div>

				</div>
				<div id="step7" class="fullInfo">
					<div class="video" style="background: url('<?php echo get_template_directory_uri(); ?>/img/Move7.jpg') center no-repeat; background-size: cover;"></div>
					<div>
						<h1>Pirouette en dehors</h1>
						<h4>(peer-WET)</h4>
						<!-- <h5>	French, literally, teetotum<br/>
						First Known Use: 1706</h5> -->
						<p>Pirouette - Whirl or spin. A complete turn of the body on one foot either on the pointe or demi- pointe, the motive power being obtained from a combination of plie and arm movement. There is a great variety of pirouettes.<br/>
						en dehors - Outward. The leg moves in a circular direction, clockwise. As for example, in rond de jambe en dehors.<br/>(ahn duh-AWR) </p>
						<hr>
						<a href="javascript:void(0)"><h5>Suggested Music</h5></a>
						<a href="javascript: void(0)"><h5>Dance Example</h5></a>
					</div>

				</div>
				<div id="step8" class="fullInfo">
					<div class="video" style="background: url('<?php echo get_template_directory_uri(); ?>/img/Move8.jpg') center no-repeat; background-size: cover;"></div>
					<div>
						<h1>Piqué to 1st arabesque</h1>
						<h4>(pee- KAY)</h4>
						<!-- <h5>	French, literally, teetotum<br/>
						First Known Use: 1706</h5> -->
						<p>Piqué - Pricked, pricking. Executed by stepping directly on the pointe of the working foot in any desired direction with the other foot raised in the air.<br/>
						<br/>Arabesque - One of the basic poses in ballet. It is a position of the body, in profile, supported on one leg, with the other leg extended behind and at right angles to it, and the arms held in various harmonious positions creating the longest possible line along the body.</p>
						<hr>
						<a href="javascript:void(0)"><h5>Suggested Music</h5></a>
						<a href="javascript: void(0)"><h5>Dance Example</h5></a>
					</div>

				</div>
				<div id="step9" class="fullInfo">
					<div class="video" style="background: url('<?php echo get_template_directory_uri(); ?>/img/Move9.jpg') center no-repeat; background-size: cover;"></div>
					<div>
						<h1>Chassé to 1st arabesque</h1>
						<h4>(sha- Say)</h4>
						<!-- <h5>	French, literally, teetotum<br/>
						First Known Use: 1706</h5> -->
						<p>Chassé - Chased. A step in which one foot literally chases the other out of its position.<br/>
						<br/>Arabesque - One of the basic poses in ballet. It is a position of the body, in profile, supported on one leg, with the other leg extended behind and at right angles to it, and the arms held in various harmonious positions creating the longest possible line along the body.</p>
						<hr>
						<a href="javascript:void(0)"><h5>Suggested Music</h5></a>
						<a href="javascript: void(0)"><h5>Dance Example</h5></a>
					</div>

				</div>
				
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->	

<?php
get_footer();
