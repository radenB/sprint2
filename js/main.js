/*

  KSS Boilerplate

  Site Development by Kitchen Sink Studios, Inc
  Developer: Travis Foshe

*/

$kss = [];

//1 - 2 - 9 - 4 - 6 - 7 - 3 - 5
function seqOne() {
    $('#step-01 .add-step').trigger('click');
    $('#step-02 .add-step').trigger('click');
    $('#step-09 .add-step').trigger('click');
    $('#step-04 .add-step').trigger('click');
    $('#step-06 .add-step').trigger('click');
    $('#step-07 .add-step').trigger('click');
    $('#step-03 .add-step').trigger('click');
    $('#step-05 .add-step').trigger('click');

    var temp = $('.current-dance').css('background-image');
    var newURL = temp.replace('Move1', 'Clipped_Sequence_1');
    $('.current-dance').css({'background' : newURL+' center no-repeat'});
    $('.progress').stop().css({'width' : '100%'});
}

//1 - 2 - 9 - 4 - 6 - 7 - 3 - 8
function seqTwo() {
    $('#step-01 .add-step').trigger('click');
    $('#step-02 .add-step').trigger('click');
    $('#step-09 .add-step').trigger('click');
    $('#step-04 .add-step').trigger('click');
    $('#step-06 .add-step').trigger('click');
    $('#step-07 .add-step').trigger('click');
    $('#step-03 .add-step').trigger('click');
    $('#step-08 .add-step').trigger('click');

    var temp = $('.current-dance').css('background-image');
    var newURL = temp.replace('Move1', 'Clipped_Sequence');
    $('.current-dance').css({'background' : newURL+' center no-repeat'});
    $('.progress').stop().css({'width' : '100%'});
}

$(document).ready(function(){ 

// GLOBALS -----------------------
$kss.contentWidth = $('section').outerWidth();
$kss.contentHeight = $('section').outerHeight();

// SVG linked-to-inline conversion
$('img.svg').each(function(){
    var $img = $(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    $.get(imgURL, function(data) {
        // Get the SVG tag, ignore the rest
        var $svg = $(data).find('svg');

        // Add replaced image's ID to the new SVG
        if(typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if(typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass+' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');
        
        // Check if the viewport is set, else we gonna set it if we can.
        if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
        }

        // Replace image with new SVG
        $img.replaceWith($svg);

    }, 'xml');

});

i = 0;

// SLICK INIT ---------------------------

$('.stepWrap').slick({
    slidesToShow: 3,
    autoplay: false,
    slidesToScroll: 1,
    draggable: true,
    infinite: true,
    prevArrow:'<div class="prevArrow"></div>',
    nextArrow:'<div class="nextArrow"></div>'
});

$('.step div:nth-child(2)').on('click', function(){
    console.log($(this).parent().data('name'));
    var stepNum = $(this).parent().data('name');
    var imgSRC = $('#step'+stepNum+' .video').css('background-image');
    var vidSRC = imgSRC.replace('.jpg','.gif');

     $('#step'+stepNum+' .video').css({'background-image' : vidSRC});

    $('#step'+stepNum).css({'display' : 'block', 'z-index' : '1'}).animate({'opacity' : '1'}, 750).addClass('active');
    $('.lightBox').css({'display' : 'block'}).animate({'opacity' : '1'}, 500);
});

$('.complete').on('click', function(){
    $('.reset').trigger('click');
    seqOne();
});
$('.example').on('click', function(){
    $('.reset').trigger('click');
    seqTwo();
})

$('.reset').on('click', function(){
    $('.progress-wrap .progress').css({'width' : 0});
    $('.current-dance').css({'background' : 'black'});
    $('.information ul').children().remove();
    i = 0;
})

$('.add-step').on('click', function(){
    console.log($(this).parent().data('name'));
    var stepNum = $(this).parent().data('name');
    var imgSRC = $('#step'+stepNum+' .video').css('background-image');
    var vidSRC = imgSRC.replace('.jpg','.gif');
    var step = $(this).parent().data('step');

    console.log($(this).parent().data('step'));
    $('.information ul').append('<li>'+step+'</li>');

    console.log(step);

    if(i == 0){
        console.log($('.current-dance').css('background-image'));
        $('.current-dance').css({'background-image' : vidSRC, 'background-size' : 'cover'});
    }
    console.log('+1');
    current = $('.progress-wrap .progress').width() / $('.progress-wrap').width();
    newVar = (current + .11) * 100;

    i = i+1;

    $('.progress-wrap .progress').stop().animate({'width' : newVar+'%'}, 600);
});

$('.current-dance').on('click', function(){
    console.log('BIGGER');
});

$(document).keyup(function(e) {
  if (e.keyCode == 27) {
        $('.lightBox').animate({'opacity' : '0'}, 500);
        setTimeout(function(){
            $('.lightBox').css({'display' : 'none'});
            $('.fullInfo').css({'z-index' : '-1'})
            var activeStep = $('.active .video').css('background-image');
            var oldSRC = activeStep.replace('.gif','.jpg');

            $('.active .video').css({'background-image' : oldSRC});
            $('.fullInfo').removeClass('active');
        }, 500)
    }
});

// WINDOW RESIZE FUNCTIONS -----------------------
$(window).on('resize', function() {
  windowWidth = $(window).width();
  windowHeight = $(window).height();
});

$(window).on('scroll', function() {
    // what do
});


$(window).load(function() {
    $('#loader').animate({opacity:0}, 500, function() {
        $(this).hide();
        $('#page').animate({opacity:1}, 500, function() {
        });
    })
});



});